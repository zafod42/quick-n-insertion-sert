#include <iostream>
#include "Quicksort.h"
#include <chrono>
#include <ctime>

#define SIZE 2000000

template<class T>
void printArray(std::ostream& out, T* arr, size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		out << arr[i] << ' ';
	}
	out << '\n';
}

template<class T>
void antiQSort(T* arr, size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		arr[i] = i;
	}

	for (size_t i = 0; i < size; ++i) {
		swap(arr[i], arr[i / 2]);
	}
}

template<class T>
void goodQSort(T* arr, size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		arr[i] = 1;
	}
}

template<class T>
void printTest(T* arr, size_t size)
{
	static int testN = 0;
	++testN;
	auto start = std::chrono::steady_clock::now();
	improvedQuickSort(arr, size);
	auto end = std::chrono::steady_clock::now();

	std::cout << "Test " << testN << '\n';
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "\tSorted in " << ms.count() << " milliseconds\n";

	std::cout << "\tComprasions: " << comprasions << '\n';
	std::cout << "\tMoves: " << moves << '\n';
	std::cout << "\tMax actual recursion depth: " << max_calls << '\n';

}

int main()
{
	int test[20] = { 4, 4, 5, 7, 6, 7 ,2, 5, 5, 3, 5, 10, 3, 10 , 7, 7, 7, 4, 4, 7};
	std::cout << "Before sort: ";
	printArray(std::cout, test, 20);
	improvedQuickSort(test, 20);
	std::cout << "After sort: ";
	printArray(std::cout, test, 20);
	int* goodArr = new int[SIZE];
	goodQSort(goodArr, SIZE);
	int* worstArr = new int[SIZE];
	antiQSort(worstArr, SIZE);
	int* midArr = new int[SIZE];
	for (int i = 0; i < SIZE; ++i)
	{
		midArr[i] = rand() % SIZE;
	}
	std::cout << "Array size: " << SIZE << '\n';
	std::cout << "Array size identified as small: " << SMALL_ARRAY_SIZE << '\n';
	std::cout << "Best possible recursion depth: log2(N) = " << log2(SIZE) << '\n';
	std::cout << "Avarage possible recursion depth: 2 * log_1.33(N) = " << 2 * log(SIZE) / log(4. / 3.) << '\n';
	std::cout << "Worst possible recursion depth: N - 1 = " << SIZE - 1 << '\n'
		      << "\n========================TESTS========================\n";
	printTest(goodArr, SIZE);
	delete[] goodArr;
	printTest(midArr, SIZE);
	delete[] midArr;
	printTest(worstArr, SIZE);
	delete[] worstArr;
	std::cout << "======================TESTS END======================\n\n";
	std::cout << "Tests description: \nTest 1 - best case, array consist of equal elements\nTest 2 - avarage case, used rand() for elements\nTest 3 - worst case";
	std::cout << " made with anti quicksort function\n         wich makes quicksort choose max element as a pivot on each step\n";
	return 0;

}