#ifndef __QUICKSORT_H
#define __QUICKSORT_H

#define SMALL_ARRAY_SIZE 10

int comprasions = 0;
int moves = 0;
int max_calls = 0;

template<class T>
void swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}

template<class T>
void insertionSort(T* arr, size_t size)
{
	int j = 0;
	for (size_t i = 0; i <= size; ++i)
	{
		j = i - 1;
		while (j >= 0 && arr[j] < arr[j + 1])
		{
			swap(arr[j + 1], arr[j]);
			++moves;
			--j;
		}
		++comprasions;
	}
}

template<class T>
void limitedQuickSort(T* arr, size_t left, size_t right, size_t limit, int calls)
{
	if (calls > max_calls)
	{
		max_calls = calls;
	}
	// skip small arrays
	if(right - left <= limit)
	{
		return;
	}
	// partition
	size_t i = left;
	size_t j = right;
	int pivot = arr[(i + j) / 2];

	while (i <= j) {
		while (arr[i] > pivot) ++i;
		while (arr[j] < pivot) --j;
		comprasions += 2;
		if (i >= j) break;
		swap(arr[i++], arr[j--]);
		++moves;
	}
	// recursive part of algorithm
	if (j > left) limitedQuickSort(arr, left, j, limit, calls + 1);
	if (i < right) limitedQuickSort(arr, i, right, limit, calls + 1);
	
}

template<class T>
void improvedQuickSort(T* arr, size_t size)
{
	comprasions = 0;
	moves = 0;
	max_calls = 0;
	limitedQuickSort(arr, 0, size - 1, SMALL_ARRAY_SIZE, 0);
	insertionSort(arr, size - 1);
}


#endif // !__QUICKSORT_H
